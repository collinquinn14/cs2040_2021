#
# nn.py: Basic Neural Network implementation stub.
# You will fill out the stubs below using numpy as much as possible.
# This class serves as a base for you to build on for the labs.
#
# Author: Derek Riley 2021 and Collin Quinn
# Inspired by https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6
# Note: the 80/20 split functionality occurs mostly in the driver class
#

import numpy as np
import matplotlib.pyplot as plt


def sigmoid(x):
    """This is the sigmoid activation function."""
    return 1.0 / (1.0 + np.exp(-1.0 * x))


def sigmoid_derivative(x):
    """This is the derivative of the sigmoid function."""
    return sigmoid(x) * (1.0 - sigmoid(x))


def make_epochs_list(epochs):
    """Makes a list starting at 1 until the total number of epochs. This is used to build the plot"""
    epochs_list = []
    for i in range(epochs):
        epochs_list.append(i)

    return epochs_list


def generate_plot(epochs, output_list, type_plot):
    """Generates a matplotlib plot with epochs vs loss"""
    title = ''
    if type_plot == 'accuracy':
        title = 'Epochs vs. Accuracy'
    elif type_plot == 'precision':
        title = 'Epochs vs. Precision'
    else:
        title = 'Epochs vs. Loss'
    epochs_num = epochs
    epochs_list = make_epochs_list(epochs_num)
    x = epochs_list
    y = output_list
    plt.plot(x, y)
    plt.title(title)
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.show()


def test_training(output_list):
    """This method is one additional test that demonstrates the network is training correctly.
    :param: List output_list: Loss values per epoch
    """
    maximum = max(output_list)
    minimum = min(output_list)

    if maximum > minimum:
        return True
    else:
        return False


class NeuralNetwork:
    """Represents a basic fully connected single-layer neural network.

    Attributes:
        _train_x (2D numpy array): input features, one row for each sample,
            and one column for each feature
        weights_2 (numpy array): connection weights between the input
            and hidden layer
        weights_3 (numpy array): connection weights between the hidden
            layer and output neuron
        y (numpy array): expected outputs of the network, one row for each
            sample, and one column for each output variable
        output (numpy array): stores the current output of the network
            after a feedforward pass
        learning_rate (float): scales the derivative influence in backprop
    """

    def __init__(self, x, y, num_hidden_neurons=4, lr=1):
        """Setup a Neural Network with a single hidden layer.  This method
        requires two vectors of x and y values as the input and output data.
        """

        self._hidden_neurons = num_hidden_neurons
        self._train_x = x
        self._y = y
        self._layer_1 = np.zeros((self._y.shape[0], num_hidden_neurons))
        self._output = np.zeros(self._y.shape)

        # weights/biases for test_nn
        self._weights_2 = np.array([[ 3.07153357,  2.01940447, -2.14695621,  2.62044111],
                                    [ 2.83203743,  2.15003442, -2.16855273,  2.77165525]])

        self._weights_3 = np.array([[ 3.8124126 ],
                                    [ 1.92454886],
                                    [-5.20663292],
                                    [ 3.21598943]])
        self._biases_2 = np.array([-1.26285168, -0.72768134,  0.89760201, -1.10572122])
        self._biases_3 = np.array([-2.1110666])

        # default
        self._learning_rate = lr

    def load_4_layer_ttt_network(self):

        self._weights_2 = np.random.rand(self._train_x.shape[1], self._hidden_neurons)
        self._weights_3 = np.random.rand(self._hidden_neurons, 1)  # 4 = num_hidden_neurons
        self._biases_2 = np.random.rand(self._train_x.shape[0], self._hidden_neurons)
        self._biases_3 = np.random.rand(self._train_x.shape[0], 1)

    def inference(self):
        """
        Use the network to make predictions for a given vector of inputs.
        This is the math to support a feedforward pass.
        """

        dot_1 = np.dot(self._train_x, self._weights_2)
        layer1 = sigmoid(dot_1 + self._biases_2)

        dot_2 = np.dot(layer1, self._weights_3)
        return sigmoid(dot_2 + self._biases_3)

    def inference_8020_helper(self, line):
        """This is used as a secondary inference method for the 80/20 train/test sample"""

        dot_1 = np.dot(line, self._weights_2)
        layer1 = sigmoid(dot_1 + self._biases_2)

        dot_2 = np.dot(layer1, self._weights_3)
        return sigmoid(dot_2 + self._biases_3)

    def feedforward(self):
        """
        This is used in the training process to calculate and save the
        outputs for backpropogation calculations.
        """
        self._output = self.inference()

    def backprop(self):
        """
        Update model weights based on the error between the most recent
        predictions (feedforward) and the training values.
        # Note: Utilized https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6
        and https://www.youtube.com/watch?v=ZVi647MYeXU to assist in generation of this method
        """

        derivative_loss = 2 * (self._y - self._output) # Lx = 1/2 (y(x) - a^L)^2
        output_error_delta = derivative_loss * sigmoid_derivative(self._output)

        deriv_weights2 = np.dot(self._layer_1.T, output_error_delta)
        deriv_biases2 = output_error_delta

        output_error_delta = np.dot(output_error_delta, self._weights_3.T) * sigmoid_derivative(self._layer_1)
        deriv_weights1 = np.dot(self._train_x.T, output_error_delta)
        deriv_biases1 = output_error_delta

        # update the weights and biases
        self._weights_2 += self._learning_rate * deriv_weights1
        self._weights_3 += self._learning_rate * deriv_weights2
        self._biases_2 += self._learning_rate * deriv_biases1
        self._biases_3 += self._learning_rate * deriv_biases2

    def train(self, epochs=100, verbose=0):
        """This method trains the network for the given number of epochs.
        It doesn't return anything, instead it just updates the state of
        the network variables.
        """
        accuracy_list = []
        precision_list = []
        output_list = []
        for i in range(epochs):
            self.feedforward()
            self.backprop()
            accuracy, precision = self.accuracy_precision()
            accuracy_list.append(accuracy)
            precision_list.append(precision)
            print('accuracy/precision = ' + str(self.accuracy_precision()))
            if verbose > 1:
                # print("Epoch: " + str(i) + " ------- Loss: " + str(self.loss()))
                output_list.append(self.loss())

        # generate_plot(epochs, output_list, 'outputs')
        # generate_plot(epochs, accuracy_list, 'accuracy')
        # generate_plot(epochs, precision_list, 'precision')
        is_training = test_training(output_list)
        if is_training is False:
            raise Exception('Your network is not training properly!')

    def loss(self):
        """ Calculate the MSE error for the set of training data."""

        loss = np.mean(np.square(self._output - self._y))
        return loss

    def accuracy_precision(self):
        """Calculate and return the accuracy and precision. This
        assumes that the network has already been trained.
        """

        threshold = 0.8
        true_p = np.sum(np.logical_and(self._y == 1, self._output > threshold))
        true_n = np.sum(np.logical_and(self._y == 0, self._output <= threshold))
        false_p = np.sum(np.logical_and(self._y == 1, self._output <= threshold))
        false_n = np.sum(np.logical_and(self._y == 0, self._output > threshold))

        accuracy = (true_p + true_n) / (true_p + true_n + false_p + false_n)
        precision = true_p / (true_p + false_p)
        return accuracy, precision
