from typing import *
import numpy as np
import nnwk6TWO
import random

epochs = 200

def create_or_nn_data():
    # input training data set for OR
    x = np.array([[0,0],
                [0,1],
                [1,0],
                [1,1]])
    # expected outputs corresponding to given inputs
    y = np.array([[0],
                [1],
                [1],
                [1]])
    return x,y


def create_and_nn_data():
    # input training data set for AND
    x = np.array([[0,0],
                [0,1],
                [1,0],
                [1,1]])
    # expected outputs corresponding to given inputs
    y = np.array([[0],
                [0],
                [0],
                [1]])
    return x,y


def load_tictactoe_csv(filepath):

    boards = []
    labels = []
    with open(filepath) as fl:
        for ln in fl:
            cols = ln.strip().split(",")
            board = [0 if s == "o" else 1 for s in cols[:-1]]
            label = 0 if cols[-1] == "Xwin" else 1
            labels.append([label])
            boards.append(board)
    x = np.array(boards)
    y = np.array(labels)
    return x, y


def test_or_nn(verbose=0):
    x,y = create_or_nn_data()
    nn = nnwk6TWO.NeuralNetwork(x, y, 4, 1)
    nn.feedforward()
    if verbose > 0:
        print("OR 1 " + str(nn.loss()))
        print("NN output " + str(nn._output))
        print(nn.accuracy_precision())
        # print('nn loss = ' + str(nn.loss()))
    assert nn.loss() < .04


def test_ttt_nn(verbose=0):
    x, y = load_tictactoe_csv("tic-tac-toeFull.csv")
    nn = nnwk6TWO.NeuralNetwork(x, y, 4, .5)
    nn.load_4_layer_ttt_network()
    nn.feedforward()

    if verbose > 0:
        nn.train(epochs, verbose)
        counter_incorrect = 0
        counter_correct = 0
        total = 0
        out = nn._output
        for i in range(len(nn._output)):
            if out[i] > .5 and y[i] == 1:
                total += 1 - out[i]
                counter_correct += 1
            elif out[0] < .5 and y[i] == 0:
                total += 0 + out[i]
                counter_correct += 1
            else:
                counter_incorrect += 1
        accuracy, precision = nn.accuracy_precision()
        print('Accuracy = ' + str(accuracy))
        print('Precision = ' + str(precision))
        print('Number correct: ' + str(counter_correct))
        print('Number incorrect: ' + str(counter_incorrect))
        print('Accuracy in predictions =  ' + str(total / 78))
        print("Final Loss = " + str(nn.loss()))

    assert nn.loss() < .02


def test_ttt_nn_8020(verbose):
    x, y = load_tictactoe_csv("tic-tac-toeFullTraining.csv")
    nn = nnwk6TWO.NeuralNetwork(x, y, 4, .1)
    nn.load_4_layer_ttt_network()
    nn.train(200, verbose)
    boards = []
    labels = []
    with open("tic-tac-toeFullValidation.csv") as file:
        for line in file:
            cols = line.strip().split(",")
            board = []
            for s in cols[:-1]:
                if s == "o":
                    board += [0]
                elif s == "x":
                    board += [1]
                else:
                    board += [2]
            label = [0] if cols[-1] == "Xwin" else [1]
            labels.append(label)
            boards.append(board)
    lines = np.array(boards)
    outputs = np.array(labels)
    counter_correct = 0
    counter_incorrect = 0
    total = 0

    for i in range(len(lines)):
        num = nn.inference_8020_helper(lines[i])

        if num[i][0] > .5 and outputs[i] == 1:
            total += 1 - outputs[i]
            counter_correct += 1
        elif num[i][0] < .5 and y[i] == 0:
            total += 0 + outputs[i]
            counter_correct += 1
        else:
            counter_incorrect += 1

    print('Number correct ' + str(counter_correct))
    print('Number incorrect ' + str(counter_incorrect))
    print('Accuracy in predictions: ' + str(counter_correct / len(outputs)))
    print('Loss = ' + str(nn.loss()))
    assert nn.loss() < .02


def make_epochs_list(epochs):
    epochs_list = []
    for i in range(epochs):
        epochs_list.append(i)

    return epochs_list


def run_all(verbose=0) -> None:
    """Runs all test cases"""
    test_or_nn(verbose)
    test_ttt_nn(verbose)
    test_ttt_nn_8020(verbose)
    print("All tests pass.")


def split_dataset() -> None:
    """Splits the tic-tac-toeFull into a training and validation set"""
    training_file = open("tic-tac-toeFullTraining.csv", "w")
    validation_file = open("tic-tac-toeFullValidation.csv", "w")
    with open("tic-tac-toeFull.csv") as file:
        for line in file:
            num = random.random()
            if num > .8:
                validation_file.write(line)
            else:
                training_file.write(line)
    training_file.close()
    validation_file.close()


def main() -> int:
    """Main test program which prompts user for tests to run and displays any
    result.
    """
    verbose = int(input("Enter 0-2 for verbosity (0 is quiet, 2 is everything):"))
    n = int(input("Enter test number (1-3; 0 = run all): "))
    if n == 0:
        run_all(verbose)
        return 0
    elif n == 1:
        result = test_or_nn(verbose)
    elif n == 2:
        result = test_ttt_nn(verbose)
    elif n == 3:
        split_dataset()
        result = test_ttt_nn_8020(verbose)
    else:
        print("Error: unrecognized test number " + str(n))
    print("Test passes with result " + str(result))
    return 0


if __name__ == "__main__":
    main()