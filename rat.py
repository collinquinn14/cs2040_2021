#
# CS2400 Introduction to AI
# rat.py
#
# Spring, 2020
#
# Author: Collin Quinn, Keegan Rhodes
#
# Stub class for Lab 2
# This class creates a Rat agent to be used to explore a Dungeon
#
# Note: Instance variables with a single preceeding underscore are intended
# to be protected, so setters and getters are included to enable this convention.
#
# Note: The -> notation in the function definition line is a type hint.  This
# will make identifying the appropriate return type easier, but they are not
# enforced by Python.
#
from queue import Queue

from dungeon import Room, Dungeon
from typing import *


class Rat:
    """Represents a Rat agent in a dungeon. It enables navigation of the
    dungeon space through searching.

    Attributes:
         dungeon (Dungeon): identifier for the dungeon to be explored
         echo_rooms_searched (bool): used as a state identifier for searching rooms
         start_loc (Room): identifier for current location of the rat
         rooms_visited_prev_search (List[str]): contains a list of rooms that have already been searched
    """

    def __init__(self, dungeon: Dungeon, start_location: Room):
        """ This constructor stores the references when the Rat is
        initialized. """
        self._dungeon = dungeon
        self._rooms_visited_prev_search = [Type[str]]
        self._start_loc = start_location
        self._echo_rooms_searched = False

    @property
    def dungeon(self) -> Dungeon:
        """ This function returns a reference to the dungeon.  """
        return self._dungeon

    def set_echo_rooms_searched(self) -> None:
        """ The _self_rooms_searched variable is used as a flag for whether
        the rat should display rooms as they are visited. """
        self._echo_rooms_searched = True

    def path_to(self, target_location: Room) -> List[Room]:
        """ This function finds and returns a list of rooms from
        start_location to target_location.  The list will include
        both the start and destination, and if there isn't a path
        the list will be empty. This function uses depth first search. """
        self._rooms_visited_prev_search = []
        possibilities = [self._start_loc]
        prev = {Type[Room]: Type[Room]}
        while len(possibilities) != 0:
            next_room = possibilities.pop()
            self._rooms_visited_prev_search.append(next_room.name)
            if self._echo_rooms_searched:
                print("Visiting: " + next_room.name)
            if next_room is target_location:
                path = [next_room]
                while next_room is not self._start_loc:
                    path.append(prev[next_room])
                    next_room = prev[next_room]
                path.reverse()
                return path
            else:
                for individual_room in reversed(next_room.neighbors()):
                    if individual_room not in prev.values():
                        prev[individual_room] = next_room
                        possibilities.append(individual_room)
        return []

    def directions_to(self, target_location: Room) -> List[str]:
        """ This function returns a list of the names of the rooms from the
            start_location to the target_location. """
        rooms_all = self.path_to(target_location)
        room_names = []
        for individual_room in rooms_all:
            room_names.append(individual_room.name)
        return room_names

    def bfs_directions_to(self, target_location: Room) -> List[str]:
        """Return the list of rooms names from the rat's current location to
        the target location. Uses breadth-first search."""
        rooms_all = self.bfs_path_to(target_location)
        room_names = []
        for individual_room in rooms_all:
            room_names.append(individual_room.name)
        return room_names

    def bfs_path_to(self, target_location: Room) -> List[Room]:
        """Returns the list of rooms from the start location to the
        target location, using breadth-first search to find the path."""
        possibilities: Queue[Room] = Queue()
        possibilities.put(self._start_loc)
        visited_set = set()
        visited_set.add(self._start_loc)
        prev = {Type[Room]: Type[Room]}
        while not possibilities.empty():
            next_room = possibilities.get()
            self._rooms_visited_prev_search.append(next_room.name)
            if self._echo_rooms_searched:
                print("Visiting: " + next_room.name)
            if next_room is target_location:
                path = [next_room]
                while next_room is not self._start_loc:
                    path.append(prev[next_room])
                    next_room = prev[next_room]
                path.reverse()
                return path
            else:
                for individual_room in next_room.neighbors():
                    if individual_room not in visited_set:
                        visited_set.add(individual_room)
                        prev[individual_room] = next_room
                        possibilities.put(individual_room)
        return []

    def dfs_limit(self, depth: int, target_location: Room):
        """Private helper method used in iterative deepening search.
              Returns the list of rooms from the start location to the
              target location. This uses depth first search up to a specified depth."""
        frontier = [[self._start_loc]]
        visited_set = set()
        while len(frontier) != 0:
            path = frontier.pop()
            room = path[-1]
            if room.name not in visited_set:
                if self._echo_rooms_searched:
                    print("Visiting: " + room.name)
                if room.name == target_location.name:
                    return path
                visited_set.add(room.name)
                neighbors = room.neighbors().__reversed__()
                for room in neighbors:
                    new_path = path.copy()
                    new_path.append(room)
                    if len(new_path) - 1 <= depth:
                        frontier.append(new_path)
        return []

    def id_directions_to(self, target_location: Room) -> List[str]:
        """Return the list of rooms names from the rat's current location to
        the target location. Uses iterative deepening."""
        path = self.id_path_to(target_location)
        names = []
        for room in path:
            names.append(room.name)
        return names

    def id_path_to(self, target_location: Room) -> List[Room]:
        """Returns the list of rooms from the start location to the
        target location, using iterative deepening."""
        path = []
        x = 1
        while (x <= self._dungeon.size()) & (len(path) == 0):
            path = self.dfs_limit(x, target_location)
            x = x + 1
        return path

    def astar_directions_to(self, target_location: Room) -> List[str]:
        """
        Return the list of rooms names from the rat's current location to the target location. Uses A* search.
        Author: Collin Quinn
        """
        rooms_all = self._astar_search(target_location)
        room_names = []
        for room in rooms_all:
            name = room.name
            if name.find(',') != -1:  # accounts for having to manipulate the Room name for case/test # 7
                # (i.e. Room name of '0,15' must become '15,0' - ex. see test_lab_4.py ->
                # d.add_room(Room(str(row) + "," + str(col), 1, col, row))
                new_list = []
                x = name.split(',')
                new_list.append(x[1])
                new_list.append(',')
                new_list.append(x[0])
                z = "".join(new_list)
                room_names.append(z)
            else:
                room_names.append(name)
        return room_names

    def astar_path_to(self, target_location: Room) -> List[Room]:
        """
        Returns the list of rooms from the start location to the target location, using A* search to find the path.
        Author: Keegan Rhodes
        """
        return self._astar_search(target_location)

    def _astar_search(self, target_location: Room) -> List[Room]:
        """
        Helper method for astar_path_to; utilizes A* search to return the path
        Authors: Keegan Rhodes, Collin Quinn
        """
        possibilities = [self._start_loc]
        visited_set = set()
        visited_set.add(self._start_loc)
        prev = {Type[Room]: Type[Room]}
        candidates = []
        names = {}

        while len(possibilities) > 0:
            next_room = possibilities.pop(0)
            self._rooms_visited_prev_search.append(next_room.name)
            if next_room is target_location:
                path = [next_room]

                while next_room is not self._start_loc:
                    path.append(prev[next_room])
                    next_room = prev[next_room]
                path.reverse()
                return path
            else:
                for individual_room in next_room.neighbors():
                    if individual_room not in visited_set:
                        visited_set.add(individual_room)
                        prev[individual_room] = next_room
                        possibilities.append(individual_room)

                for room in possibilities:
                    if room not in visited_set:
                        room_contents = [room.name, room.estimated_cost_to(self._start_loc),
                                         room.estimated_cost_to(target_location)]
                        names[room.name] = room
                        candidates.append(room_contents)

                lowest_cost = 40
                reference = next_room
                for candidate in candidates:
                    if len(candidates) > 0:
                        if candidate[2] < lowest_cost:
                            candidate[2] = lowest_cost
                            reference = names[candidate[0]]
                        names.clear()
                        candidates.clear()
                        possibilities.clear()
                        possibilities.append(reference)

        return []

    def rooms_visited_by_last_search(self) -> List[str]:
        """ Return the current list of rooms visited (in any order). This is used by Esubmit to validate the
         current rooms visited.
         Author: Collin Quinn
         """
        parsed_list = []
        for element in self._rooms_visited_prev_search:
            if type(element) == str:
                parsed_list.append(element)
        return parsed_list
