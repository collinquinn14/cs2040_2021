# This code was modified from https://towardsdatascience.com/bbn-bayesian-belief-networks-how-to-build-them-effectively-in-python-6b7f93435bba

import pandas as pd # for data manipulation 
import networkx as nx # for drawing graphs
import matplotlib.pyplot as plt # for drawing graphs

# for creating Bayesian Belief Networks (BBN)
from pybbn.graph.dag import Bbn
from pybbn.graph.edge import Edge, EdgeType
from pybbn.graph.jointree import EvidenceBuilder
from pybbn.graph.node import BbnNode
from pybbn.graph.variable import Variable
from pybbn.pptc.inferencecontroller import InferenceController

# Set Pandas options to display more columns
pd.options.display.max_columns=50

# Read in the weather data csv
df=pd.read_csv('weatherAUS.csv', encoding='utf-8')

# Drop records where target RainTomorrow=NaN
df=df[pd.isnull(df['RainTomorrow'])==False]

# For other columns with missing values, fill them in with column mean
df=df.fillna(df.mean())

# Create bands for variables that we want to use in the model
df['Humidity9amCat']=df['Humidity9am'].apply(lambda x: '1.>60' if x>60 else '0.<=60')
df['Humidity3pmCat']=df['Humidity3pm'].apply(lambda x: '1.>60' if x>60 else '0.<=60')

# added values for demo
df['Pressure9amCat'] = df['Pressure9am'].apply(lambda x: '1.>1005' if x>60 else '0.<=1005')
df['Pressure3pmCat'] = df['Pressure3pm'].apply(lambda x: '1.>1005' if x>60 else '0.<=1005')

# Show a snapshot of data
# df.head()

# Create nodes by manually typing in probabilities
P9am = BbnNode(Variable(3, 'P9am', ['<=1005', '>1005']), [0.32000, 0.68000]) # 1
P3pm = BbnNode(Variable(4, 'P3pm', ['<=1005', '>1005']), [0.90000, 0.05000, 0.45000, 0.60000]) # 2
H9am = BbnNode(Variable(0, 'H9am', ['<=60', '>60']), [0.30658, 0.69342]) #1
H3pm = BbnNode(Variable(1, 'H3pm', ['<=60', '>60']), [0.92827, 0.07173, 0.55760, 0.44240]) # 2
RT = BbnNode(Variable(2, 'RT', ['No', 'Yes']), [0.92314, 0.07686,
                                                0.89072, 0.10928,
                                                0.76008, 0.23992,
                                                0.64250, 0.35750,
                                                0.49168, 0.50832,
                                                0.32182, 0.67818]) # 6


# Create Network
bbn = Bbn() \
    .add_node(H9am) \
    .add_node(H3pm) \
    .add_node(RT) \
    .add_node(P9am) \
    .add_node(P3pm) \
    .add_edge(Edge(H9am, H3pm, EdgeType.DIRECTED)) \
    .add_edge(Edge(H3pm, RT, EdgeType.DIRECTED)) \
    .add_edge(Edge(P3pm, RT, EdgeType.DIRECTED)) \
    .add_edge(Edge(P9am, P3pm, EdgeType.DIRECTED)) \


# Convert the BBN to a join tree
join_tree = InferenceController.apply(bbn)

# Set node positions
pos = {0: (-1, 2), 1: (-1, 1), 2: (-0.5, 0), 3: (0, 2), 4: (0, 1)}

# Set options for graph looks
options = {
    "font_size": 16,
    "node_size": 4000,
    "node_color": "white",
    "edgecolors": "black",
    "edge_color": "red",
    "linewidths": 5,
    "width": 5, }

# Generate graph
n, d = bbn.to_nx_graph()
nx.draw(n, with_labels=True, labels=d, pos=pos, **options)

# Update margins and print the graph
ax = plt.gca()
ax.margins(0.10)
plt.axis("off")
plt.show()


# Define a function for printing marginal probabilities
def print_probs():
    for node in join_tree.get_bbn_nodes():
        potential = join_tree.get_bbn_potential(node)
        print("Node:", node)
        print("Values:")
        print(potential)
        print('----------------')


# Use the above function to print marginal probabilities
# print_probs()


# To add evidence of events that happened so probability distribution can be recalculated
def evidence(nod, cat, val):
    ev = EvidenceBuilder() \
        .with_node(join_tree.get_bbn_node_by_name(nod)) \
        .with_evidence(cat, val) \
        .build()
    join_tree.set_observation(ev)
    return ev


# Use above function to add evidence'
evidence('H9am', '>60', 1.0)
evidence('P9am', '>1005', 1.0)

# Print marginal probabilities
print_probs()
print('----NOW INCLUDING 3PM DATA-----')

# Add more evidence
evidence('H3pm', '>60', 1.0)
evidence('P3pm', '>1005', 1.0)

# Print marginal probabilities
print_probs()

